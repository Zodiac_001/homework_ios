protocol Trigger {
    var rifle: Rifle { get set }
    func fire()
}

protocol Rifle {
    func removeSafety()
}


final class Receiver: Trigger {
    var rifle: Rifle

    func fire() {
        self.rifle.removeSafety()
    }
    
    init(rifle: Rifle) {
        self.rifle = rifle
    }
}

final class M4A1: Rifle {
    func removeSafety() {
        print("M4A1 is shooting");
    }
}

final class AK47: Rifle {
    func removeSafety() {
        print("AK47 is shooting")
    }
}

let m4a1 = Receiver(rifle: M4A1())
m4a1.fire()

let ak47 = Receiver(rifle: AK47())
ak47.fire()