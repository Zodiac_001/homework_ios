class SingletonSwift{
    static let sharedInstance = SingletonSwift()
    var instanceName : String?
    
    private init(){
        print("Singleton initialized")
    }
}

let single1 = SingletonSwift.sharedInstance
single1.instanceName = "This is the name of this instance"

let single2 = SingletonSwift.sharedInstance

print(single2.instanceName!)