protocol DotaUnits {
    var name : String {get set}
    func spawn()
    func attack()
    func move()
}

class Hero : DotaUnits{
    var name: String
    init(HeroName heroname : String) {
        self.name = heroname
    }
    func spawn() {
        print("\(name) has spawned")
    }
    func attack() {
        print("\(name) is attacking right now")
    }
    func move() {
        print("\(name) is moving to a lane")
    }
}

class Creep : DotaUnits {
    var name: String
    init(CreepName creepName : String) {
        self.name = creepName
    }
    func spawn() {
        print("Creep \(name) has spawned")
    }
    func attack() {
        print("The \(name) Creep is attacking")
    }
    func move() {
        print("The \(name) is moving")
    }
}

enum DotaUnitTypes{
    case Hero
    case Creep
}

class DotaFactory{
    private static var sharedDotaFactory = DotaFactory()
    class func shared() -> DotaFactory {
        return sharedDotaFactory
    }
    func getUnit(UnitType unitType : DotaUnitTypes, UnitName unitName : String)->DotaUnits{
        switch unitType {
        case .Hero:
            return Hero(HeroName: unitName)
        case .Creep:
            return Creep(CreepName: unitName)
        }
    }
}

let hero = DotaFactory.shared().getUnit(UnitType: .Hero, UnitName: "Huskar")
hero.attack()

let creep = DotaFactory.shared().getUnit(UnitType: .Creep, UnitName: "Ancient Dragon")
creep.spawn()