protocol SiegeTankFactory {
    func buildTrack()
    func buildEngine()
    func buildChassisArmor()
    func buildArmamentPlasmaCannons()
    func buildArmamentShockCannons()
}

class ArcliteFactory: SiegeTankFactory {
    func buildTrack() {
        print("build tracks for AAV-5 Arclite siege tank")
    }
    func buildEngine() {
        print("build engine for AAV-5 Arclite siege tank")
    }
    func buildChassisArmor() {
        print("build chassis armor for AAV-5 Arclite siege tank")
    }
    func buildArmamentPlasmaCannons() {
        print("build Twin 80mm PPG-7 plasma cannons for AAV-5 Arclite siege tank")
    }
     func buildArmamentShockCannons() {
        print("build 120mm Mjolnir shock/artillery cannon for AAV-5 Arclite siege tank")
    }
    
}
class CrucioFactory: SiegeTankFactory {
    func buildTrack() {
        print("build tracks for Crucio siege tank")
    }
    func buildEngine() {
        print("build engine for Crucio siege tank")
    }
    func buildChassisArmor() {
        print("build chassis armo for Crucio siege tank")
    }
    func buildArmamentPlasmaCannons() {
        print("build Twin 90mm plasma cannons for Crucio siege tank")
    }
    func buildArmamentShockCannons() {
        print("build 180mm shock cannon for Crucio siege tank")
    }
}

class TerranSiegeTankFactory {
    var factory: SiegeTankFactory
    init(factory: SiegeTankFactory) {
        self.factory = factory
    }
    func assemble() {
        factory.buildTrack()
        factory.buildEngine()
        factory.buildChassisArmor()
        factory.buildArmamentPlasmaCannons()
        factory.buildArmamentShockCannons()
    }
}

let arcliteEngineer = TerranSiegeTankFactory(factory: ArcliteFactory())
arcliteEngineer.assemble()
let crucioEngineer = TerranSiegeTankFactory(factory: CrucioFactory())
crucioEngineer.assemble()